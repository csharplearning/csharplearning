﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Diagnostics;
using System.IO;

namespace ConsoleApplication4
{
    class Runshell
    {
       public static void Main(string[] args)
        {
            ProcessStartInfo psi = new ProcessStartInfo();
         
            psi.FileName = "/tmp/bash.sh";
            psi.UseShellExecute = false;
            psi.RedirectStandardOutput = true;

            psi.Arguments = "test";
            Process p = Process.Start(psi);
            string strOutput = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
            Console.WriteLine(strOutput);
        }


    }
}
